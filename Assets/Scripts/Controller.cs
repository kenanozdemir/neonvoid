﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	public GameObject player;

	public AudioClip swipe1;
	public AudioClip swipe2;
	public AudioClip swipe3;

	// swipe variables
	private Vector3 startPosition = Vector3.zero;
	private Vector3 endPosition = Vector3.zero;
	public float swipeDistance = 0.1f;

	//rotation variables
	public float speed = 55.0f;
	public static Quaternion qTo = Quaternion.identity;
	private float treshold = 1.0f;

	// "way" variable holds this info: "direction of the rotation". 0=no_action 1=right 2=left 3=downleft 4=upright 5=upleft 6=downright 
	private int way=0; 

	void Awake(){
		player.transform.rotation = Quaternion.identity;
		qTo = Quaternion.identity;
	}

	void Update () {

		// gets swipe info
		if (Input.GetMouseButtonDown(0)) startPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); // swipe begins
		if (Input.GetMouseButtonUp(0)) endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); // swipe ends

		// click (not swiping) doesn't count
		if (startPosition.x == endPosition.x && startPosition.y == endPosition.y) startPosition = endPosition = Vector3.zero;

		// when swipe occurs this determines the direction
		if (startPosition != endPosition && startPosition != Vector3.zero && endPosition != Vector3.zero) {

			float deltaX = endPosition.x - startPosition.x; //horizontal movement info
			float deltaY = endPosition.y - startPosition.y; //vertical movement info

			print ("StartX : " + startPosition.x + " EdnX : " + endPosition.x + " StartY : " + startPosition.y + " EndY : " + endPosition.y);
			print ("deltaX : " + deltaX + "    deltaY : " + deltaY);


			if ((deltaX < -0.5f) && (deltaY > 0.3f) && (startPosition.y < endPosition.y)) way = 5; //upleft
			else if ((deltaX > 0.5f) && (deltaY < -0.3f) && (startPosition.y > endPosition.y)) way = 6; //downright
			else if ((deltaX > -0.5f) && (deltaY > 0.3f) && (startPosition.y < endPosition.y)) way = 4; //upright
			else if ((deltaX < 0.5f) && (deltaY < -0.3f) && (startPosition.y > endPosition.y)) way = 3; //downleft
			else if ((deltaX > 0.5f) && (deltaY >= -0.3f && deltaY <= 0.3f)) way = 1; //right
			else if ((deltaX < -0.5f) && (deltaY >= -0.3f && deltaY <= 0.3f)) way = 2; //left

			print (way);
			startPosition = endPosition = Vector3.zero;
		}
			
		//Changes rotation depending "way" variable.
		if (Quaternion.Angle (player.transform.rotation, qTo) > treshold) {
			player.transform.rotation = Quaternion.RotateTowards (player.transform.rotation, qTo, Time.deltaTime * speed);
		}
		else {
			player.transform.rotation = qTo;
			Quaternion qSave;

			if (way==6) {
				way = 0;
				qSave = player.transform.rotation;
				player.transform.RotateAround (player.transform.position, Vector3.back, 90.0f);
				qTo = player.transform.rotation;
				player.transform.rotation = qSave;
				AudioSource audio = GetComponent<AudioSource> ();
				GetComponent<AudioSource> ().clip = swipe3;
				GetComponent<AudioSource> ().volume = 0.2f;
				audio.Play ();
			}
			else if (way==5) {
				way = 0;
				qSave = player.transform.rotation;
				player.transform.RotateAround (player.transform.position, Vector3.forward, 90.0f);
				qTo = player.transform.rotation;
				player.transform.rotation = qSave;
				AudioSource audio = GetComponent<AudioSource> ();
				GetComponent<AudioSource> ().clip = swipe3;
				GetComponent<AudioSource> ().volume = 0.2f;
				audio.Play ();
			}
			else if (way==4) {
				way = 0;
				qSave = player.transform.rotation;
				player.transform.RotateAround (player.transform.position, Vector3.right, 90.0f);
				qTo = player.transform.rotation;
				player.transform.rotation = qSave;
				AudioSource audio = GetComponent<AudioSource> ();
				GetComponent<AudioSource> ().clip = swipe2;
				GetComponent<AudioSource> ().volume = 0.2f;
				audio.Play ();
			}
			else if (way==3) {
				way = 0;
				qSave = player.transform.rotation;
				player.transform.RotateAround (player.transform.position, Vector3.left, 90.0f);
				qTo = player.transform.rotation;
				player.transform.rotation = qSave;
				AudioSource audio = GetComponent<AudioSource> ();
				GetComponent<AudioSource> ().clip = swipe2;
				GetComponent<AudioSource> ().volume = 0.2f;
				audio.Play ();
			}
			else if (way==2) {
				way = 0;
				qSave = player.transform.rotation;
				player.transform.RotateAround (player.transform.position, Vector3.up, 90.0f);
				qTo = player.transform.rotation;
				player.transform.rotation = qSave;
				AudioSource audio = GetComponent<AudioSource> ();
				GetComponent<AudioSource> ().clip = swipe1;
				GetComponent<AudioSource> ().volume = 0.2f;
				audio.Play ();
			}
			else if (way==1) {
				way = 0;
				qSave = player.transform.rotation;
				player.transform.RotateAround (player.transform.position, Vector3.down, 90.0f);
				qTo = player.transform.rotation;
				player.transform.rotation = qSave;
				AudioSource audio = GetComponent<AudioSource> ();
				GetComponent<AudioSource> ().clip = swipe1;
				GetComponent<AudioSource> ().volume = 0.2f;
				audio.Play ();
			}
		}

	}
}
