﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using PrimitivePlus;

public class Builder : MonoBehaviour {

	public GameObject controller;

	public GameObject player;
	public static int check = 0;

	public Material blue;

	public GameObject cube;
	public GameObject prismTriangle;
	public GameObject cylinder;

	public GameObject showScore;
	public static int score=0;

	public GameObject gameOver;
	public GameObject retry;

	public float speed = 0.1f;

	private int rowCube;
	private int rowPrismTriangle;
	private int rowCylinder;

	void Awake () {
		score = 0;
		cube.transform.position = new Vector3 (0, 11, 0);
		cube.transform.rotation = Quaternion.identity;
		showScore.GetComponent<Text> ().text = "Score: 0";
		check = 0;

		LevelItem levelItem = new LevelItem (LevelItem.Type.CUBE, 0.05f);
	}

	void Update () {

		if (check == 0) {
				cube.transform.Translate (0, -speed, 0);
			if (cube.transform.position == Vector3.zero) {
				check = Random.Range(0,3);
				if (check==1) cube.transform.position = new Vector3 (11, 0, 0);
				if (check==2) cube.transform.position = new Vector3 (0, 0, -11);
				if (check==0) cube.transform.position = new Vector3 (0, 11, 0);
				showScore.GetComponent<Text> ().text = "Score: " + ++score;
			}
		}
		if (check == 1) {
				cube.transform.Translate (-speed, 0, 0);
			if (cube.transform.position == Vector3.zero) {
				check = Random.Range(0,3);
				if (check==1) cube.transform.position = new Vector3 (11, 0, 0);
				if (check==2) cube.transform.position = new Vector3 (0, 0, -11);
				if (check==0) cube.transform.position = new Vector3 (0, 11, 0);
				showScore.GetComponent<Text> ().text = "Score: " + ++score;
			}
		}
		if (check == 2) {
				cube.transform.Translate (0, 0, speed);
			if (cube.transform.position == Vector3.zero) {
				check = Random.Range(0,3);
				if (check==1) cube.transform.position = new Vector3 (11, 0, 0);
				if (check==2) cube.transform.position = new Vector3 (0, 0, -11);
				if (check==0) cube.transform.position = new Vector3 (0, 11, 0);
				showScore.GetComponent<Text> ().text = "Score: " + ++score;
			}
		}
		if (check == 3) {
			check = 5;
			gameOver.SetActive (true);
			retry.SetActive (true);
			controller.SetActive (false);
		}
		if (check == 4) {
			score = 0;
			showScore.GetComponent<Text> ().text = "Score: 0";
			gameOver.SetActive (false);
			retry.SetActive (false);
			cube.transform.position = new Vector3 (0, 11, 0);
			cube.transform.rotation = Quaternion.identity;
			Controller.qTo = Quaternion.identity;
			player.transform.rotation = Quaternion.identity;
			controller.SetActive (true);
			cube.AddComponent<Rigidbody> ();
			cube.GetComponent<Rigidbody> ().useGravity = false;
			cube.AddComponent<BoxCollider> ();
			check = 0;
		}
	}
	private GameObject CreateCube () {
		GameObject cloneCube = Instantiate (cube, transform.position, transform.rotation) as GameObject;
		rowCube++;
		return cloneCube;
	}

	private GameObject PrismTriangle () {
		GameObject clonePrismTriangle = Instantiate (prismTriangle, transform.position, transform.rotation) as GameObject;
		rowPrismTriangle++;
		return clonePrismTriangle;
	}

	private GameObject Cylinder () {
		GameObject cloneCylinder = Instantiate (cylinder, transform.position, transform.rotation) as GameObject;
		rowCylinder++;
		return cloneCylinder;
	}
		
}
