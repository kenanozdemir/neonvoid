﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level {

	public LevelItem[] levelItemList;

	public Level(LevelItem[] levelItemList)
	{
		this.levelItemList = levelItemList;
	}
}
