﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

	public void ChangeLevel(string s){
		SceneManager.LoadScene (s);
	}

	public void TryAgain(){
		Builder.score = 0;
		Builder.check = 4;
	}
}
