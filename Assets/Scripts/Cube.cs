﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour {

	void OnCollisionEnter(Collision col){
		if(col.collider.transform.tag == "Player"){
			Destroy (GetComponent<Rigidbody> ());
			Destroy (GetComponent<BoxCollider> ());
			Builder.check = 3;
		}
	}
}
