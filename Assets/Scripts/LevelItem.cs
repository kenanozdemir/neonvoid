﻿using System.Collections;
using System.Collections.Generic;

public class LevelItem {

	public enum Type{CUBE, SPHERE}

	private float delayTime;
	private Type type;

	public LevelItem(Type type, float delayTime)
	{
		this.type = type;
		this.delayTime = delayTime;
	}
}
