﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelList : MonoBehaviour {

	public Level[] levelList;
	void Awake () {
		LevelItem item1 = new LevelItem (LevelItem.Type.CUBE, 0.01f);
		LevelItem[] levelItemList = { item1 };
		Level level = new Level (levelItemList);
		levelList [0] = level;

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
